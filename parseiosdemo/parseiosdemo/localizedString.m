//
//  localizedString.m
//  parseiosdemo
//
//  Created by XuLiang on 2017/2/13.
//  Copyright © 2017年 XuLiang. All rights reserved.
//

#import <Foundation/Foundation.h>
int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        
        //////////////////
        NSString *dataStr = @"\"GWIDERR\" = \"Gateway ID error, please check!\";\"GWPWERR\" = \"Password error, please enter again!\";";
        NSString *pattern = @"\"[^\"]+\" =";
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:nil];
        NSArray *results = [regex matchesInString:dataStr options:NSMatchingWithoutAnchoringBounds range:NSMakeRange(0, dataStr.length)];
        if (results.count > 0) {
            for (NSTextCheckingResult *checkresult in results) {
                NSString *localStringStr = [dataStr substringWithRange:checkresult.range];
                NSString *pattern2 = @"\"[^\"]+\"";
                NSRegularExpression *regex2 = [NSRegularExpression regularExpressionWithPattern:pattern2 options:0 error:nil];
                NSTextCheckingResult *localCheckingResult = [regex2 firstMatchInString:localStringStr options:0 range:NSMakeRange(0, localStringStr.length)];
                
                if (localCheckingResult.range.location != NSNotFound) {
                    NSString *LocalizedString = [[localStringStr substringWithRange:localCheckingResult.range] stringByReplacingOccurrencesOfString:@"\"" withString:@""];
                    NSLog(@"%@",LocalizedString);
                }
            }
        }
        //////////////////
//        NSFileManager *fm = [NSFileManager defaultManager];
//        
//        NSData *datas = [fm contentsAtPath:@"/Users/xuliang/Documents/IosWorkSpace/parseiosdemo/UserManager.m"];
//        NSString *dataStr = [[NSString alloc] initWithData:datas encoding:NSUTF8StringEncoding];//@"NSLocalizedString(@\"04-B1-USER-NAME\", nil)";//
//        NSString *pattern = @"NSLocalizedString\s*\\(\s*@\"[^\"]+\"";
//        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:nil];
//        NSArray *results = [regex matchesInString:dataStr options:NSMatchingWithoutAnchoringBounds range:NSMakeRange(0, dataStr.length)];
//        if (results.count > 0) {
//            for (NSTextCheckingResult *checkresult in results) {
//                NSLog(@"%llu,%lu,%lu",checkresult.resultType,(unsigned long)checkresult.range.location,(unsigned long)checkresult.range.length);
//                NSString *localStringStr = [dataStr substringWithRange:checkresult.range];
//                NSString *pattern2 = @"\"[^\"]+\"";
//                NSRegularExpression *regex2 = [NSRegularExpression regularExpressionWithPattern:pattern2 options:0 error:nil];
//                NSTextCheckingResult *localCheckingResult = [regex2 firstMatchInString:localStringStr options:0 range:NSMakeRange(0, localStringStr.length)];
//                
//                if (localCheckingResult.range.location != NSNotFound) {
//                    NSLog(@"%@", [localStringStr substringWithRange:localCheckingResult.range]);
//                    NSString *LocalizedString = [[localStringStr substringWithRange:localCheckingResult.range] stringByReplacingOccurrencesOfString:@"\"" withString:@""];
//                    //找出文件中的国际化比对
//                    //如果 找不到 输出文件名 key名
//                    NSArray *keyArry = @[@"04-B1-USER-NAME",@"BUTTONS"];
//                    if (LocalizedString && [keyArry containsObject:LocalizedString]){
//                        
//                    }else{
//                        NSLog(@"不存在Key“%@”",LocalizedString);
//                    }
//                }
//            }
//            
//        }
        
        
        return 0;
    }
}
